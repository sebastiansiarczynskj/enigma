package com.drzewo;


public abstract class TreeTemplate {
    private final String subgenus;
    private Integer leafNumber;
    private Integer branchNumber;
    private Integer age;
    public TreeTemplate(String subgenus, Integer leafNumber, Integer branchNumber, Integer age) {
        this.subgenus = subgenus;
        this.leafNumber = leafNumber;
        this.branchNumber = branchNumber;
        this.age = age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public void setBranchNumber(Integer branchNumber) {
        this.branchNumber = branchNumber;
    }

    public Integer getBranchNumber() {
        return branchNumber;
    }

    public void setLeafNumber(Integer leafNumber) {
        this.leafNumber = leafNumber;
    }

    public Integer getLeafNumber() {
        return leafNumber;
    }

    public String getSubgenus() {
        return subgenus;
    }

    public abstract void grow();
}
