package com.drzewo.Degame;

import java.util.Optional;

public interface DegameInterface {
    void bloom();
    Optional getFruits();
}
