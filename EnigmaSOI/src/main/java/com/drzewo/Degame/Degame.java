package com.drzewo.Degame;

import com.drzewo.TreeTemplate;

import java.util.Optional;

public class Degame extends TreeTemplate implements DegameInterface{
    private Integer fruits;
    public Degame(String subgenus, Integer leafNumber, Integer branchNumber, Integer age) {
        super(subgenus, leafNumber, branchNumber, age);
        fruits = null;
    }

    @Override
    public void grow() {

    }

    @Override
    public void bloom() {

    }

    @Override
    public Optional<Integer> getFruits() {
        if (fruits == null) return Optional.empty();
        return Optional.of(fruits);
    }

}
