package com.drzewo.Conifer;

import com.drzewo.TreeTemplate;

import java.util.Optional;

public class Conifer extends TreeTemplate implements ConiferInterface {
    private Integer cones;
    public Conifer(String subgenus, Integer leafNumber, Integer branchNumber, Integer age) {
        super(subgenus, leafNumber, branchNumber, age);
        cones = null;
    }

    @Override
    public void grow() {

    }

    @Override
    public void bloom() {

    }

    @Override
    public Optional<Integer> getCone() {
        if (cones == null) return Optional.empty();
        return Optional.of(cones);
    }
}
