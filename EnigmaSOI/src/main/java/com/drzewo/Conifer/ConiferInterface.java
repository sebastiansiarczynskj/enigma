package com.drzewo.Conifer;

import java.util.Optional;

public interface ConiferInterface {
    void bloom();
    Optional getCone();
}
