package rekrutacja.enigmasoi.rekrutacja.DeviceLocation;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DeviceLocationControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;

    private JSONObject reqBody;

    @BeforeEach
    public void setUpMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }
    @AfterEach
    public void unSetUpMockMvc() {
        mockMvc = null;
    }
    @BeforeEach
    public void setUpReq() throws JSONException {
        reqBody = new JSONObject();
        reqBody.put("device_token", 1234);
        reqBody.put("latitude", "51.4345");
        reqBody.put("longitude", "76.3456");
        reqBody.put("date", "2022-01-12");
    }
    @Test
    public void addLocation() throws Exception {
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(201));
    }

    @Test
    public void addLocationWrongReqConf() throws Exception {
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX2lkIjoxLCJpc3MiOiJlbmlnbWEtdGFzayJ9.XpUGJTQi3jKWr-eK9JLz0myvBHcjHg5RZ93qIfKQh20").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(401));
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_ATOM_XML).content(reqBody.toString())).andExpect(status().is(415));
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMzQsImlzcyI6ImVuaWdtYS10YXNrIn0.TcTQEGdfRw6ACllfGLcJoqlp7TFuQ_kMsDG3VEFEhK0").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(401));
    }

    @Test
    public void addLocationWrongBody() throws Exception {
        reqBody = new JSONObject();
        reqBody.put("device_token", 111);
        reqBody.put("location", 1);
        reqBody.put("date", null);
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(400));

        reqBody = new JSONObject();
        reqBody.put("device_token", 111);
        reqBody.put("location", 1);
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(400));
    }

    @Test
    public void getLocationForDevice() throws Exception {
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(201));

        mockMvc.perform(get("/device-location/get/1234").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(200));
    }

    @Test
    public void getLocationForWrongDevice() throws Exception {
        mockMvc.perform(get("/device-location/get/12345").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(400));
    }

    @Test
    public void getLocationForDeviceWithLimit() throws Exception {
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(201));
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(201));
        mockMvc.perform(post("/device-location/add").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(201));

        mockMvc.perform(get("/device-location/get/1234/5").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(200));
    }

    @Test
    public void getLocationForWrongDeviceLimit() throws Exception {
        mockMvc.perform(get("/device-location/get/12345/2").header(HttpHeaders.AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZGV2aWNlX3Rva2VuIjoxMjM0LCJpc3MiOiJlbmlnbWEtdGFzayJ9.CATLG6U6jnvCSmNV3FvXL5jkoZKI2axuzvXMDQ4oPVg").contentType(MediaType.APPLICATION_JSON).content(reqBody.toString())).andExpect(status().is(400));
    }
}
