package rekrutacja.enigmasoi.rekrutacja.Device;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface DeviceRepo extends CrudRepository<DeviceModel, Integer> {
    @Query(value = "SELECT * FROM device WHERE device_token = :token", nativeQuery = true)
    Optional<DeviceModel> findOneByDeviceToken(@Param("token") @NotNull Integer device_token);
}
