package rekrutacja.enigmasoi.rekrutacja.DeviceLocation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import rekrutacja.enigmasoi.rekrutacja.Config.BeansConfig;
import rekrutacja.enigmasoi.rekrutacja.DTO.LocationDTO;

import javax.validation.Valid;
import java.sql.SQLException;
import java.time.format.DateTimeParseException;
import java.util.*;


/**
 * Kontroler odpowiedzialny za obsluge endpointa zwiazanego z dodawaniem lokalizacji urzadzenia
 * @author Sebastian Siarczyński
 */
@RequestMapping(value = "/device-location", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class DeviceLocation {
    @Autowired
    private DeviceLocationService deviceLocationService;
    @Autowired
    private BeansConfig beansConfig;

    /**
     * Metoda dodawajaca lokalizacje urzadzenia po podanym tokenie urzadzenia
     * @param locationDTO Obiekt DTO z danymi lokalizacji i urzadzenia
     * @return JSON z informacja zwrotna co do stanu zapisu danych w bazie
     */
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> addDeviceLocation(@Valid @RequestBody LocationDTO locationDTO) {
        beansConfig.getLogger().info("New location has been send: " + locationDTO);
        final Map<String, Object> response = new HashMap<>();
        try {
            deviceLocationService.addDeviceLocation(locationDTO);
            response.put("success", true);
            return ResponseEntity.status(201).body(response);
        } catch (SQLException | DateTimeParseException | IllegalArgumentException e) {
            beansConfig.getLogger().fatal(e.toString());
            response.put("success", false);
            response.put("message", e.getMessage());
            return ResponseEntity.status(400).body(response);
        }
    }

    /**
     * Metoda odpowiadajaca za zwrocenie danych lokalizacji dla danego urzadzenia z podanym tokenem
     * @param device_token Token urzadzenia
     * @return JSON z danymi
     */
    @GetMapping(value = "/get/{device_token}")
    public ResponseEntity<Map<String, Object>> getAllLocationForDevice(@PathVariable Integer device_token) {
        return deviceLocationService.findAllForDeviceToken(device_token);
    }

    /**
     * Metoda odpowiedajaca za zwracanie danych lokalizacji dla danego urzadzenia tylko tym razem z limitem na ilosc odpowiedzi
     * @param device_token token urzadzenia
     * @param limit limit odpowiedzi
     * @return JSON z danymi
     */
    @GetMapping(value = "/get/{device_token}/{limit}")
    public ResponseEntity<Map<String, Object>> getLocationForDeviceWithLimit(@PathVariable Integer device_token, @PathVariable Integer limit) {
        return deviceLocationService.findAllForDeviceTokenWithLimit(device_token, limit);
    }
}
