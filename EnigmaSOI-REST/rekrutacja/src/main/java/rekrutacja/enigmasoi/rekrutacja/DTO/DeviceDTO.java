package rekrutacja.enigmasoi.rekrutacja.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@AllArgsConstructor
public class DeviceDTO {
    @NotNull
    private Integer id;
    @NotNull
    private String type;
    @NotNull
    private Integer token;
}
