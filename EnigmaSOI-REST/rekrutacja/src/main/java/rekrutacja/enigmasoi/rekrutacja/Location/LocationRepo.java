package rekrutacja.enigmasoi.rekrutacja.Location;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface LocationRepo extends CrudRepository<LocationModel, Integer> {
    Optional<List<LocationModel>> findAllByDeviceId(@NotNull Integer device_id);
    @Query(value = "SELECT * FROM location WHERE device_id = :device_id LIMIT :limit", nativeQuery = true)
    Optional<List<LocationModel>> findAllByDeviceIdWithLimit(@NotNull @Param("device_id") Integer device_id, @NotNull @Param("limit") Integer limit);
}
