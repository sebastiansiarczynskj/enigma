package rekrutacja.enigmasoi.rekrutacja.WebAuth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import rekrutacja.enigmasoi.rekrutacja.Config.BeansConfig;
import rekrutacja.enigmasoi.rekrutacja.Device.DeviceModel;
import rekrutacja.enigmasoi.rekrutacja.Device.DeviceRepo;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Configuration
public class JwtAuth extends OncePerRequestFilter {
    @Autowired
    private DeviceRepo deviceRepo;
    @Autowired
    private BeansConfig beansConfig;
    @Autowired
    private JwtClass jwtClass;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getHeader(HttpHeaders.AUTHORIZATION) == null || !request.getHeader(HttpHeaders.AUTHORIZATION).startsWith("Bearer ")) {
            filterChain.doFilter(request, response);
            beansConfig.getLogger().warn("REQUEST WITHOUT TOKEN!");
            return;
        }
        final String token = request.getHeader(HttpHeaders.AUTHORIZATION).split(" ")[1];
        Optional<Integer> device_token = jwtClass.verifyToken(token);
        if (device_token.isEmpty()) {
            filterChain.doFilter(request, response);
            beansConfig.getLogger().warn("UNAUTHORIZED ATTEMPT TO CONNECT!");
            return;
        }

        Optional<DeviceModel> device = deviceRepo.findOneByDeviceToken(device_token.get());

        if (device.isEmpty()) {
            filterChain.doFilter(request, response);
            beansConfig.getLogger().warn("UNAUTHORIZED ATTEMPT TO CONNECT!");
            return;
        }
        beansConfig.getLogger().info("USER WITH ID: " + device.get().getId() + " HAS MADE A REQUEST");
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(device.get(), null, null);
        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        filterChain.doFilter(request, response);
    }
}
