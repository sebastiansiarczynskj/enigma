package rekrutacja.enigmasoi.rekrutacja.Device;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "DEVICE")
@Table(indexes = {
        @Index(name = "idx_devicemodel_unq", columnList = "device_token", unique = true)
})
@Getter
@Setter
public class DeviceModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 45)
    @NotNull
    private String type;

    @NotNull
    @Column(unique = true)
    private Integer device_token;
}
