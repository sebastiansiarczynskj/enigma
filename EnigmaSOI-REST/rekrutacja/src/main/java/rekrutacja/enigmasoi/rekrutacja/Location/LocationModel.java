package rekrutacja.enigmasoi.rekrutacja.Location;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rekrutacja.enigmasoi.rekrutacja.Device.DeviceModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity(name = "LOCATION")
@Table(indexes = {
        @Index(name = "idx_locationmodel_id_unq", columnList = "id, device_id", unique = true)
})
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LocationModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @ManyToOne
    private DeviceModel device;

    @NotNull
    @Column(length = 90)
    private String latitude;

    @NotNull
    @Column(length = 90)
    private String longitude;

    @NotNull
    private LocalDate date;
}
