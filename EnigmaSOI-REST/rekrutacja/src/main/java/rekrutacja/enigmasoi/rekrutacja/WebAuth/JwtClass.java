package rekrutacja.enigmasoi.rekrutacja.WebAuth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rekrutacja.enigmasoi.rekrutacja.Config.BeansConfig;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Component
public class JwtClass {
    @Autowired
    private BeansConfig beansConfig;

    public Optional<Integer> verifyToken(@NotNull String token) {
        try {
            final Algorithm algorithm = Algorithm.HMAC256("enigma");
            final JWTVerifier jwtVerifier = JWT.require(algorithm).withIssuer("enigma-task").build();
            DecodedJWT verify = jwtVerifier.verify(token);
            Integer device_token = verify.getClaim("device_token").asInt();
            return Optional.of(device_token);
        } catch (JWTVerificationException | NullPointerException e) {
            beansConfig.getLogger().fatal(e.toString());
            return Optional.empty();
        }
    }
}
