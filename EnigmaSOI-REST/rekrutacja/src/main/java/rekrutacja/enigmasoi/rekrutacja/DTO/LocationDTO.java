package rekrutacja.enigmasoi.rekrutacja.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@AllArgsConstructor
public class LocationDTO {
    private Integer id;
    private DeviceDTO device;
    @NotNull
    private Integer device_token;
    @NotNull
    private String latitude;
    @NotNull
    private String longitude;
    @NotNull
    private String date;
}
