package rekrutacja.enigmasoi.rekrutacja.DeviceLocation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rekrutacja.enigmasoi.rekrutacja.Config.BeansConfig;
import rekrutacja.enigmasoi.rekrutacja.DTO.DeviceDTO;
import rekrutacja.enigmasoi.rekrutacja.DTO.LocationDTO;
import rekrutacja.enigmasoi.rekrutacja.Device.DeviceModel;
import rekrutacja.enigmasoi.rekrutacja.Device.DeviceRepo;
import rekrutacja.enigmasoi.rekrutacja.Location.LocationModel;
import rekrutacja.enigmasoi.rekrutacja.Location.LocationRepo;

import javax.validation.constraints.NotNull;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;

/**
 * Serwis odpowiedzialny za obsluge logiki biznesowej do endpointa odpowiedzialnego za dodawanie lokalizacji urzadzenia
 * @author Sebastian Siarczyński
 */
@Service
public class DeviceLocationService {
    @Autowired
    private DeviceRepo deviceRepo;
    @Autowired
    private LocationRepo locationRepo;
    @Autowired
    private BeansConfig beansConfig;

    /**
     * Metoda dodająca lokalizacje urządzenia
     * @param locationDTO Obiekt DTO z danymi lokalizacji
     * @throws SQLException Gdy wystapil blad SQL
     * @throws DateTimeParseException Gdy parsowanie daty sie nie powiodlo
     * @throws IllegalArgumentException Gdy zostal uzyty zly token
     */
    @Transactional
    public void addDeviceLocation(@NotNull LocationDTO locationDTO) throws SQLException, DateTimeParseException, IllegalArgumentException {
        Optional<DeviceModel> oneByDeviceToken = deviceRepo.findOneByDeviceToken(locationDTO.getDevice_token());
        if (oneByDeviceToken.isEmpty()) {
            beansConfig.getLogger().fatal("There is no device with this device_token: " + locationDTO.getDevice_token());
            throw new IllegalArgumentException("Wrong device token!");
        }
        LocationModel locationModel = new LocationModel();
        locationModel.setDevice(oneByDeviceToken.get());
        locationModel.setDate(LocalDate.parse(locationDTO.getDate()));
        locationModel.setLatitude(locationDTO.getLatitude());
        locationModel.setLongitude(locationDTO.getLongitude());
        LocationModel save = locationRepo.save(locationModel);
        beansConfig.getLogger().info("New location has been saved with ID: " + save.getId());
    }

    /**
     * Metoda pobierajaca wszystkie rekordy lokalizacji dla danego urzadzenia
     * @param device_token token urzadzenia
     * @return Dane lokalizacji
     */
    public ResponseEntity<Map<String, Object>> findAllForDeviceToken(@NotNull Integer device_token) {
        Optional<DeviceModel> oneByDeviceToken = deviceRepo.findOneByDeviceToken(device_token);
        if (oneByDeviceToken.isEmpty()) {
            final Map<String, Object> response = new HashMap<>();
            response.put("success", false);
            response.put("message", "There is no device with this token");
            beansConfig.getLogger().fatal("Trying to reach device with wrong token");
            return ResponseEntity.status(400).body(response);
        }
        Optional<List<LocationModel>> allByDeviceId = locationRepo.findAllByDeviceId(oneByDeviceToken.get().getId());
        if (allByDeviceId.isEmpty()) {
            final Map<String, Object> response = new HashMap<>();
            response.put("success", true);
            response.put("message", "There is no location for this device");
            beansConfig.getLogger().fatal("Trying to reach device with no location records");
            return ResponseEntity.status(200).body(response);
        }
        final List<LocationModel> locationModels = allByDeviceId.get();
        final List<LocationDTO> dtos = new ArrayList<>();
        locationModels.forEach(item -> {
            dtos.add(new LocationDTO(item.getId(), new DeviceDTO(item.getDevice().getId(), item.getDevice().getType(), item.getDevice().getDevice_token()), item.getDevice().getDevice_token(), item.getLatitude(), item.getLongitude(), item.getDate().toString()));
        });
        final Map<String, Object> response = new HashMap<>();
        response.put("success", true);
        response.put("data", dtos);
        return ResponseEntity.status(200).body(response);
    }

    /**
     * Metoda pobierajaca te same dane co metoda wyzej jednak z limitem na ilosc rekordow, zastosowalem tutaj praktyke WET
     * @param device_token token urzadzenia
     * @param limit limit rekordow
     * @return Dane dla urzadzenia
     */
    public ResponseEntity<Map<String, Object>> findAllForDeviceTokenWithLimit(@NotNull Integer device_token, @NotNull Integer limit) {
        Optional<DeviceModel> oneByDeviceToken = deviceRepo.findOneByDeviceToken(device_token);
        if (oneByDeviceToken.isEmpty()) {
            final Map<String, Object> response = new HashMap<>();
            response.put("success", false);
            response.put("message", "There is no device with this token");
            beansConfig.getLogger().fatal("Trying to reach device with wrong token");
            return ResponseEntity.status(400).body(response);
        }
        Optional<List<LocationModel>> allByDeviceIdWithLimit = locationRepo.findAllByDeviceIdWithLimit(oneByDeviceToken.get().getId(), limit);
        if (allByDeviceIdWithLimit.isEmpty()) {
            final Map<String, Object> response = new HashMap<>();
            response.put("success", true);
            response.put("message", "There is no location for this device");
            beansConfig.getLogger().fatal("Trying to reach device with no location records");
            return ResponseEntity.status(200).body(response);
        }
        final List<LocationModel> locationModels = allByDeviceIdWithLimit.get();
        final List<LocationDTO> dtos = new ArrayList<>();
        locationModels.forEach(item -> {
            dtos.add(new LocationDTO(item.getId(), new DeviceDTO(item.getDevice().getId(), item.getDevice().getType(), item.getDevice().getDevice_token()), item.getDevice().getDevice_token(), item.getLatitude(), item.getLongitude(), item.getDate().toString()));
        });
        final Map<String, Object> response = new HashMap<>();
        response.put("success", true);
        response.put("data", dtos);
        return ResponseEntity.status(200).body(response);
    }
}
